# Based in latest Sonarqube LTS
FROM sonarqube:8.9.0-community

LABEL maintainer="Gabriel Arellano <gabrielarellano@gmail.com>"

#HEALTHCHECK --interval=5m --start-period=2m \
#    CMD test $(curl -su "admin:$SONARQUBE_ADMIN_PASSWORD" ${SONARQUBE_URL:-http://localhost:9000}/api/system/health | jq '(.health)') = '"GREEN"'

USER root

# Download SonarQube plugins
ADD https://github.com/spotbugs/sonar-findbugs/releases/download/4.0.3/sonar-findbugs-plugin-4.0.3.jar \
    https://github.com/dependency-check/dependency-check-sonar-plugin/releases/download/2.0.8/sonar-dependency-check-plugin-2.0.8.jar \
    https://github.com/Inform-Software/sonar-groovy/releases/download/1.7/sonar-groovy-plugin-1.7.jar \
    https://github.com/mc1arke/sonarqube-community-branch-plugin/releases/download/1.8.0/sonarqube-community-branch-plugin-1.8.0.jar \
    https://github.com/cnescatlab/sonar-hadolint-plugin/releases/download/1.0.0/sonar-hadolint-plugin-1.0.0.jar \
    https://github.com/sbaudoin/sonar-yaml/releases/download/v1.5.2/sonar-yaml-plugin-1.5.2.jar \
    https://github.com/sleroy/sonar-slack-notifier-plugin/releases/download/8.0.0-beta/sonar-slack-notifier-8.0.0-beta.jar \
    /opt/sonarqube/extensions/plugins/

# Configure SonarQube
RUN chown -R sonarqube:sonarqube extensions/ \
    # Disable SonarQube telemetry
    && sed -i 's/#sonar\.telemetry\.enable=true/sonar\.telemetry\.enable=false/' /opt/sonarqube/conf/sonar.properties \
    # Options needed by sonarqube-community-branch-plugin
    && echo 'sonar.web.javaAdditionalOpts=-javaagent:./extensions/plugins/sonarqube-community-branch-plugin-1.8.0.jar=web' >> /opt/sonarqube/conf/sonar.properties \
    && echo 'sonar.ce.javaAdditionalOptions=-javaagent:./extensions/plugins/sonarqube-community-branch-plugin-1.8.0.jar=ce' >> /opt/sonarqube/conf/sonar.properties \
    # Set list of patterns matching Dockerfiles (for Hadolint plugin)
    && echo 'sonar.lang.patterns.dockerfile=Dockerfile,Dockerfile.*' >> /opt/sonarqube/conf/sonar-scanner.properties

# Switch back to an unpriviledged user
USER sonarqube

